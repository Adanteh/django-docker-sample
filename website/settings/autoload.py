import os

settings = os.environ.get("DJANGO_ENV", "development")
if settings == "development":
    from .development import *  # noqa
elif settings == "production":
    from .production import *  # noqa
else:
    raise Exception("Unknown settings environment. Please set DJANGO_ENV")
