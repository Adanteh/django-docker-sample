import os
from .common import *  # noq
from .common import BASE_DIR

SECRET_KEY = config("DJANGO_SECRET_KEY")
DEBUG = True

# fmt: off
# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3')
    }
}
# fmt: on
