# -*- coding: utf-8 -*-
"""
WSGI config for arma_terrain project.

It exposes the WSGI callable as a module-level variable named ``application``.

https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os
import sys
from django.core.wsgi import get_wsgi_application

from pathlib import Path

FOLDER = Path(__file__).parent
for folder in (FOLDER, FOLDER.parent):
    if str(folder) not in sys.path:
        sys.path.insert(0, str(folder))

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings.autoload')
application = get_wsgi_application()
