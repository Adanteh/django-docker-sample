# -*- coding: utf-8 -*-
"""
https://docs.djangoproject.com/en/3.0/topics/http/urls/
"""

from django.urls import include, path, reverse
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static


admin.site.site_header = "Sample website"
admin.site.site_title = "Does some stuff"


urlpatterns = [path("admin/", admin.site.urls),] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
