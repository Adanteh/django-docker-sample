from django.urls import path
from django.contrib.auth.decorators import login_required

from . import views

app_name = "myapp"
urlpatterns = [
    path("", login_required(views.SampleView.as_view()), name=""),
]
