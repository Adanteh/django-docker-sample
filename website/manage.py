#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys

from pathlib import Path

FOLDER = Path(__file__).parent
for folder in (FOLDER, FOLDER.parent):
    if str(folder) not in sys.path:
        sys.path.insert(0, str(folder))


def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings.autoload')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
