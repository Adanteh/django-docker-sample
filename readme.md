## Readme
Basic sample project of a dockerized Django website.
Works with both SQLITE3 or PostgreSQL. The default setup is meant for ARM devices.

## Caddy
This is setup around running ANOTHER docker-compose.yml file with your Caddy proxies.
This allows you to host multiple websites on a single box for different (sub)domains
our caddy is using a separate container that will read the labels we assign to this,
this means we can keep the setting related to each websites container in the docker-compose
instead of spreading them out between different files